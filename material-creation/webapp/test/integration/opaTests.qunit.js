/* global QUnit */

sap.ui.require(["materialcreation/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
